import logging
import re
import os
import yaml
import json
import subprocess as sp
import requests

from pathlib import Path


"""
    This file (+build.py and test.py) holds all complexity regarding building and testing
    blueprints. Current targets are:

    - meta-ts
    - meta-ewaol
    - meta-ewaol-machine
    - meta-ledge-secure

    These are all OpenEmbedded layers that require hours to build
    and lots of disk space (+120G) to store all sources and compiled artifacts.

    In the building stage an external service called Tuxsuite (tuxsuite.com) is
    used to actually build artifacts so we don't have to worry about
    disk space nor computing power allocation. All iteraction with this service
    is done via a Python API. Tuxsuite is responsible for hosting build
    artifacts for a couple of days and it provides URLs to access these resources.

    In the testing stage an external service hosted in https://qa-reports.linaro.org
    is used as a middle-man to send test job requests and collect results to/from a LAVA
    instance. Currently, the instance is hosted in https://ledge.validation.linaro.org.
    Only some of the build artifacts generated in the building stage can be tested
    due to hardware availability in LAVA labs.

    Both building and testing are executed in each component repository
    tree as well as inside this CI repository.
"""


#
#   Set up logging
#
logger = logging.getLogger()
logger.setLevel(logging.DEBUG if os.getenv("DEBUG") == "1" else logging.INFO)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


#
#   Read entire env into settings
#
class Settings:
    def __init__(self, env=os.environ, extra=[]):
        if not self.validate(extra):
            return

        longest_string = 0
        for k, v in env.items():
            if len(k) > longest_string:
                longest_string = len(k)
            setattr(self, k, v)

        if self.DEBUG:
            longest_string += 1
            for k, v in env.items():
                print(f"{k.ljust(longest_string)}{v}")

        # Pre-compute some helpers
        self.IS_CI_HOME = self.CI_PROJECT_PATH.endswith("/ci")

        self.IS_MERGE_REQUEST = self.CI_PIPELINE_SOURCE == "merge_request_event"

    def validate(self, extra_vars=[]):
        """
            Run a minimum requirement check on env vars or other
            aspects of the environment that need to be present
        """
        required_vars = [
            # Pre-defined variables coming from Gitlab-CI
            # Gitlab's APIv4 base URL
            "CI_API_V4_URL",

            # true if the job is running for a protected reference (branch or tag), empty otherwise
            "CI_COMMIT_REF_PROTECTED",

            # The first eight characters of the commit that triggered the pipeline
            "CI_COMMIT_SHORT_SHA",

            # Job's unique ID
            "CI_JOB_ID",

            # Job's name which is defined in each ci script, e.g.: build-meta-ts-qemuarm64-secureboot
            "CI_JOB_NAME",

            # Pipeline's ID
            "CI_PIPELINE_ID",

            # Event that triggered the pipeline
            # Can be push, web, schedule, api, external, chat, webide, merge_request_event, external_pull_request_event, parent_pipeline, trigger, or pipeline
            "CI_PIPELINE_SOURCE",

            # Full path of the cloned repository in the runner file system
            "CI_PROJECT_DIR",

            # Gitlab's unique project ID
            "CI_PROJECT_ID",

            # The project namespace with the project name included
            "CI_PROJECT_PATH",

            # The project URL, e.g. https://gitlab.com/Linaro/blueprints/ci
            "CI_PROJECT_URL",

            # The address of the GitLab Container Registry: registry.gitlab.com
            "CI_REGISTRY",

            # Custom variables specified in ci scripts
            # Persistent directory where build jobs will write images
            "IMAGES_DIR",

            # Base url to the blueprints CI repository
            "BLUEPRINTSCI_URL",

            # Base url to the nightly builds repository
            "NIGHTLYBUILDS_URL",
        ] + extra_vars

        self.missing = [v for v in required_vars if os.getenv(v) is None]
        if len(self.missing):
            logger.warning(f"The following environment variables are missing: {self.missing}")
            return False

        for v in required_vars:
            logger.info(f"{v}={os.getenv(v)}")

        return True

    def __getattr__(self, name):
        return os.getenv(name)


#
#   Generate a TuxSuite plan
#
def generate_tuxsuite_plan(url, yml, ref=None, branch=None, name=None, image=None):
    if type(yml) is list:
        yml = ":".join(yml)

    source = {
        "sources": {
            "kas": {
                "url": url,
                "yaml": yml,
            }
        },
    }

    if ref:
        source["sources"]["kas"]["ref"] = f"refs/tags/{ref}"
    elif branch:
        source["sources"]["kas"]["branch"] = branch

    if name:
        source["name"] = name

    # Ref: https://docs.tuxsuite.com/plan/specifications/
    plan = {
        "version": 1,
        "name": name,
        "description": "",
        "jobs": [
            {
                "bake": source,
            },
        ],
    }

    return plan


#
#   Submit a tuxsuite plan to tuxsuite.com
#
def submit_to_tuxsuite(settings, plan):
    """
        1. Parse `plan` as a temporary yaml file
        2. Submit build request to tuxsuite.com
        3. Wait for build to be ready (in the future, this will be handled by callbacks)
        4. Create a results filename based on job name and id to be used in testing stage
    """

    plan_filename = f"plan-{settings.CI_JOB_NAME}-{settings.CI_JOB_ID}.yml"
    results_filename = f"result-{settings.CI_JOB_NAME}-{settings.CI_JOB_ID}.json"
    logger.debug(f"Creating temporary file for plan ({plan_filename}) and a results file ({results_filename}) for tuxsuite to write results")
    result = False
    try:
        # Write plan to a temporary file
        plan_contents = yaml.dump(plan)
        logger.info(plan_contents)
        Path(plan_filename).write_text(plan_contents)

        # Invoke tuxsuite command and wait
        tuxsuite_cmd = [
            "tuxsuite",
            "plan",
            "--json",
            "--json-out",
            results_filename,
            plan_filename,
        ]
        logger.info(tuxsuite_cmd)
        proc = sp.Popen(tuxsuite_cmd)
        proc.wait()
        result = proc.returncode == 0
    except Exception as e:
        logger.warning(f"Could not submit build request to Tuxsuite: {e}")
    finally:
        os.remove(plan_filename)
    return result, results_filename


#
#   Download any file from URL and save it to output_filename
#
def download_file(url, output_filename=None):
    try:
        show_progress = logger.isEnabledFor(logging.INFO)
        MB = 1024 ** 2
        download = requests.get(url, stream=True)
        output_filename = Path(output_filename or os.path.basename(url))
        with output_filename.open("wb") as f:
            for chunk in download.iter_content(chunk_size=MB):
                f.write(chunk)
                if show_progress:
                    print(".", end="", flush=True)

        if show_progress:
            print(" OK")
    except requests.exceptions.HTTPError as e:
        logger.warning(f"Could not download {url}: {e}")
        return False
    return True


#
#   Download image from Tuxsuite
#
def download_image(settings, result_filename, output_directory="."):
    # TODO: add unittests to this function
    """
        1. Find `download_url` in results_filename
        2. Retrieve "download_url / dirname(IMAGE)"
        3. Iterate over files received in step-2
        4. If there's re.match(basename(IMAGE)), download it
        5. Remove any timestamp in the file name
    """

    image_path = settings.IMAGE
    image_dir = os.path.dirname(image_path)
    image_filename = os.path.basename(image_path)
    image_url = None

    # The image file name provided by IMAGE env var might be a regex pattern to the real image name
    # due to images being time-stamped
    image_real_name = None

    # There must exist only one build in the file
    logger.info(f"Looking for `download_url` in {result_filename}")
    try:
        with open(result_filename, "r") as fp:
            result = json.load(fp)

        build_id = list(result["builds"].keys())[0]
        download_url = result["builds"][build_id]["download_url"]
        settings.DOWNLOAD_URL = download_url
    except Exception as e:
        logger.warning(f"Could not find dowload_url in {result_filename}: {e}")
        return False, ""

    # Now attempt to find the image in build files in tuxsuite
    url = f'{download_url}/{image_dir}/'
    logger.info(f"Looking for {image_filename} in {url}")
    build_files = requests.get(url)
    for candidate in build_files.json()['files']:
        candidate_basename = os.path.basename(candidate['Url'])
        if re.match(f'^{image_filename}$', candidate_basename):
            logger.info(f"Found {candidate_basename}")
            image_url = f'{url}{candidate_basename}'

            # Remove eventual timestamps from filename
            image_real_name = re.sub(r"-\d{8,}", "", candidate_basename)
            if image_real_name != candidate_basename:
                logger.info(f"Image renamed (removed timestamp): {candidate_basename} -> {image_real_name}")
            break

    if image_url is None:
        logger.warning(f"Could not retrieve URL for {image_path} within {url}")
        return False, ""

    # Really download image
    logger.info(f"Downloading {image_url}")
    output_directory = Path(output_directory)
    output_directory.mkdir(exist_ok=True)
    output_filename = output_directory / image_real_name

    download_ok = download_file(image_url, output_filename=output_filename)
    return download_ok, output_filename


#
#   Generate LAVA job definition
#
def generate_lava_job_definition(settings, template_name, context={}):
    # TODO: jinja2 is only available in squad-client image
    from jinja2 import (
        Environment,
        FileSystemLoader,
    )

    try:
        templates = ['blueprints_ci/lava/templates/devices', 'blueprints_ci/lava/templates/tests']
        env = Environment(loader=FileSystemLoader(templates))

        env.globals['basename'] = os.path.basename
        env.globals['splitext'] = os.path.splitext
        context['settings'] = settings

        # The template name should be a file within blueprints_ci/lava/templates/tests
        # and we should use that as part of job name, it'll help view things in SQUAD
        test_name = os.path.basename(template_name).replace(".yaml.jinja2", "")
        context['job_name'] = f"{settings.CI_JOB_NAME}-{test_name}"

        return env.get_template(template_name).render(context)
    except Exception as e:
        logger.warning(f"Could not generate LAVA job definition using template {template_name} and context {context}: {e}")
        return None


#
#   Send a result to SQUAD telling if the build passed or failed
#
def send_build_result_to_squad(settings):
    logger.info(f"Sending build result \"{settings.BUILD_RESULT}\" to {settings.SQUAD_HOST}/{settings.SQUAD_GROUP}/{settings.SQUAD_PROJECT}/build/{settings.SQUAD_BUILD}")

    Path("/tmp/metadata.json").write_text(f"""{{
        "build_url": "{settings.DEVICE} {settings.CI_PROJECT_URL}/-/jobs/{settings.CI_JOB_ID}",
        "build artifacts": "{settings.DEVICE} {settings.DOWNLOAD_URL}",
        "build log": "{settings.DEVICE} {settings.DOWNLOAD_URL}/build.log"
    }}""")

    cmd = [
        "squad-client",
        "submit",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--environment", settings.DEVICE,
        "--result-name", "build/build",
        "--result-value", settings.BUILD_RESULT,
        "--metadata", "/tmp/metadata.json",
    ]

    logger.info(" ".join(cmd))
    proc = sp.Popen(cmd)
    proc.wait()
    return proc.returncode == 0


#
#   Send a job definition to squad, that will send to LAVA
#
def send_testjob_request_to_squad(settings, job_definition):
    with open("/tmp/definition.yml", "w") as fp:
        fp.write(job_definition)

    cmd = [
        "squad-client",
        "submit-job",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--backend", "ledge.validation.linaro.org",
        "--environment", settings.LAVA_DEVICE,
        "--definition", "/tmp/definition.yml",
    ]

    logger.info(cmd)
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    stdout, err = proc.communicate()

    if proc.returncode != 0:
        return False, None

    logger.info(f"See test job progress at {settings.SQUAD_HOST}/{settings.SQUAD_GROUP}/{settings.SQUAD_PROJECT}/build/{settings.SQUAD_BUILD}/testjobs/")

    matches = re.findall(r"SQUAD job id: (\d+)", stdout.decode() + err.decode())
    if len(matches) != 1:
        logger.warning(f"Could not obtain SQUAD job id: match returned {matches}")
        return False, None

    job_id = matches[0]
    return True, job_id


#
#   Register the next job's URL trigger to SQUAD build
#   meaning that whenever the SQUAD detects that the build finished
#   it'll make an HTTP POST request to the Gitlab job URL, thus triggering
#   the "check" job, which will fetch test results accordingly
#
def register_callback_in_squad(settings):
    job_id = find_next_stage_job_id(settings)
    check_stage_job_url = f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/jobs/{job_id}/play"

    cmd = [
        "squad-client",
        "register-callback",
        "--group", settings.SQUAD_GROUP,
        "--project", settings.SQUAD_PROJECT,
        "--build", settings.SQUAD_BUILD,
        "--url", check_stage_job_url,
    ]

    logger.info(cmd)
    proc = sp.Popen(cmd)
    proc.wait()

    return proc.returncode == 0


#
#   Use settings.SQUAD_JOB_ID to fetch test results from a JOB in LAVA
#   referenced in SQUAD.
#
def fetch_tests_from_squad(settings):
    # TODO: squad_client is only available in squad-client image
    from squad_client.core.api import SquadApi
    from squad_client.core.models import TestJob, TestRun
    from squad_client.utils import getid

    SquadApi.configure(url=settings.SQUAD_HOST)

    testjob = TestJob(settings.SQUAD_JOB_ID)
    logger.info(f"LAVA URL: {testjob.external_url}")

    if testjob.job_status != "Complete":
        logger.warning(f"Failed to run job successfully: {testjob.failure}")
        return None

    testrun = TestRun(getid(testjob.testrun))
    tests = testrun.tests(fields="id,name,status").values()
    return sorted(tests, key=lambda t: t.name)


#
#   Find the Gitlab's job id for the job in the next
#   stage after "trigger_tests" job.
#
def find_next_stage_job_id(settings):
    job_name = f"check-{settings.CI_JOB_NAME}"
    pipeline_jobs_url = f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/pipelines/{settings.CI_PIPELINE_ID}/jobs"

    response = requests.get(pipeline_jobs_url)
    if not response.ok:
        logger.warning(f"Could not retrieve id for \"{job_name}\": {response.text}")
        return None

    for job in response.json():
        if job["name"] == job_name:
            return job["id"]

    logger.warning(f"Could not retrieve id for \"{job_name}\" in {response.json()}")
    return None


#
#   Resolve OS or FIRMWARE url
#
def resolve_os_or_firmware(settings, spec):
    if spec.startswith("http"):
        return spec

    job_name, image_path = spec.split(":")

    # The URL gitlab provides needs 1 redirection to the to the actual image URL, which contains the image
    # as basename of the url, so we need to get that final location
    image_url = f"{settings.NIGHTLYBUILDS_URL}/-/jobs/artifacts/main/raw/{settings.IMAGES_DIR}/{image_path}?job={job_name}"
    logger.info(f"Resolving {spec}: GET {image_url}")
    response = requests.get(image_url, allow_redirects=False)
    if response.status_code != 302:
        return None
    return response.headers["Location"]
