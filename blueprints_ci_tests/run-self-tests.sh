#!/usr/bin/env bash

TESTING_DIR=blueprints_ci_tests

ARGS=$@
if [ -z $@ ]
then
	ARGS=${TESTING_DIR}/*.py
else
	for item in $@
	do
		ARGS="${TESTING_DIR}.${item}"
	done
fi

python3 -m unittest ${ARGS}
